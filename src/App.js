import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import './App.css';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Toast from 'react-bootstrap/Toast';
import 'react-datepicker/dist/react-datepicker.css';
import moment from 'moment';
import base64 from 'base-64';
import Results from './Results';

import DatePicker from 'react-datepicker';

class App extends React.Component {
  constructor() {
    super();
    this.state = {results: null, 
      checkInDate: new Date(), 
      nights: null, 
      selectedHotel: null,
      message: {show: false, message: ''},
      error: false,
      hotels: [
        {
          "name": "Hotel Baqueira Val de Neu",
          "id": 44069509
        },
        {
          "name": "Hotel Moderno",
          "id": 10030559
        },
        {
          "name": "Hotel Grand Luxor",
          "id": 100376478
        }
    ]}

    this.handleHotelChange = this.handleHotelChange.bind(this);
    this.handleNightsChange = this.handleNightsChange.bind(this);
    this.search = this.search.bind(this);
    this.setMessage = this.setMessage.bind(this);
    this.validateForm = this.validateForm.bind(this);
  }

  setDate(checkInDate) {
    this.setState({error: false, results: null});

    if (moment(checkInDate).isBefore(moment().startOf('day'))) {
      this.setMessage(true, "El dia introducido no puede ser anterior a la fecha de hoy");
    } else {
      this.setState({checkInDate: checkInDate});
    }
  }

  componentDidMount() {
    
  }

  handleHotelChange(event) {
    this.setState({error: false, results: null});

    event.preventDefault();
    this.setState({selectedHotel: event.target.value});
  }

  handleNightsChange(event) {
    this.setState({error: false, results: null});

    if (event.target.value > 30) {
      this.setMessage(true, 'Maximo 30 noches');
      this.setState({nights: ''});
    } else {
      this.setState({nights: event.target.value});
    }
  }

  search(event) {
    event.preventDefault();

    let username = 'user1';
    let password = 'user1Pass';
    let authString = `${username}:${password}`
    let headers = new Headers();
    let checkInDateString = moment(this.state.checkInDate).format('DD/MM/YYYY');

    if (!this.validateForm()) {
      this.setState({error: true});
    } else {
      this.setState({error: false}, () => {
        headers.set('Authorization', 'Basic ' + base64.encode(authString));

        fetch(`https://api.mirai.com/MiraiWebService/availableRate/get?hotelId=${this.state.selectedHotel}&checkin=${checkInDateString}&nights=${this.state.nights}&lang=es`, {
          method: 'GET',
          headers: headers
        })
        .then((response) => {
          return response.json();
        })
        .then((results) => {
          this.setState({results: results});
        });
      });
    }
  }

  setMessage(show, message) {
    this.setState({message: {show, message}});
  }

  validateForm() {
      return this.state.selectedHotel 
        && this.state.checkInDate
        && this.state.nights
        && this.state.nights.length > 0 
        && this.state.nights > 0;
  }

  componentDidUpdate () {
    console.log("tst");
  }

  render() {
    let results = null;
    let tariffs = null;

    if (this.state.error || this.state.results) {

      if (this.state.results) {
        tariffs = this.state.results.availableRates[this.state.selectedHotel];
      }

      results = <Results error={this.state.error} tariffs={tariffs}/>
    }

    return (
      <Container>
      <Toast className="popup_message" onClose={() => this.setMessage(false, '')} show={this.state.message.show} delay={3000} autohide>
        <Toast.Header>
          <img
            src="holder.js/20x20?text=%20"
            className="rounded mr-2"
            alt=""
          />
          <strong className="mr-auto">Message</strong>
        </Toast.Header>
        <Toast.Body>{this.state.message.message}</Toast.Body>
      </Toast>
      <Row className="search-form">
      <Col>
      <Row>
          <Col>
            <h1>
              Hotels Mirai
            </h1>
          </Col>
      </Row>
      <Row>
        <Col>
         <Form.Group controlId="hotel">
            <Form.Label>Hotel:</Form.Label>
              <Form.Control as="select" onChange={this.handleHotelChange}>
                <option value="">Selecciona un hotel</option>
                {this.state.hotels.map((hotel) => <option value={hotel.id}>{hotel.name}</option>)}
            </Form.Control>
           </Form.Group>
        </Col>     
      </Row>   
      <Row>
        <Col>
          <Form.Group controlId="check-in">
            <Form.Label>Check-in:</Form.Label>
              <div/>
              <DatePicker
                dateFormat="dd/MM/yyyy"
                selected={this.state.checkInDate}
                onChange={checkInDate => this.setDate(checkInDate)}
              />
          </Form.Group>
        </Col>
      </Row>
      <Row>
        <Col>
          <Form.Group controlId="hotel">
            <Form.Label>Noches:</Form.Label>
              <Form.Control className="nigths" value={this.state.nights} type="text" onChange={this.handleNightsChange}/>
          </Form.Group>
        </Col>
      </Row>
      <Row>
        <Col>
          <Button variant="primary" onClick={this.search}>Buscar</Button>
        </Col>
      </Row>
      </Col>
      </Row>
      {results}
      </Container>
    );
  }
}

export default App;
