import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import ListGroup from 'react-bootstrap/ListGroup';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import ErrorMessage from './ErrorMessage';
import Tariff from './Tariff';

function Results(props) {
  return (
    <Row className="results">
      <Col>
        {props.tariffs && !props.error ? (
          <ListGroup as="ul">
            {props.tariffs.map((tariff) => {
              return <ListGroup.Item as="li">
                <Tariff tariff={tariff} />
              </ListGroup.Item>
            })}
          </ListGroup>
        ) : (
          <ErrorMessage message="Lo sentimos, no hay tarifas disponibles"></ErrorMessage>
        )}
      </Col>
    </Row>
  ); 
}

export default Results;