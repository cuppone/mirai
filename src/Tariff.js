import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import Badge from 'react-bootstrap/Badge';

function Tariff(props) {
  return (
    <div className="tariff">
      <div><span className="name">Nombre: </span>{props.tariff.roomName}</div>
      {props.tariff.offerName && <div><span className="name">Oferta: </span><Badge variant="success">{props.tariff.offerName}</Badge></div>}
      <div><span className="name">Régimen: </span>{props.tariff.boardName}</div>
      <div><span className="name">Ocupación: </span>Adultos: {props.tariff.occupancy.numAdults} - niños: {props.tariff.occupancy.numChilds} - bebés: {props.tariff.occupancy.numBabies}</div>
      <div><span className="name">Precio neto: </span><Badge variant="primary">{props.tariff.netPrice}</Badge></div>
    </div>
  ); 
}

export default Tariff;