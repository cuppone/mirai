import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Alert from 'react-bootstrap/Alert';

function ErrorMessage(props) {
  return (
    <Row>
      <Col>
        <Alert variant="warning">{props.message}</Alert>
      </Col>
    </Row>
  ); 
}

export default ErrorMessage;